#!/bin/bash  -i
# Place configs in filesystem
currentPath=`pwd`
# Add nvim config symlink and create symlink for backward capability with vim
echo "Placing vim and nvim configs ..."
mkdir -p ~/.config/nvim/
ln -f -s "${currentPath}/init.vim" ~/.config/nvim/init.vim
ln -f -s "${currentPath}/init.vim" ~/.vimrc
ln -f -s "${currentPath}/ideavimrc" ~/.ideavimrc

# Add tmux config
echo "Placing tmux config ..."
ln -f -s "${currentPath}/tmux.conf" ~/.tmux.conf

# Add git config
echo "Placing git configs ..."
ln -f -s "${currentPath}/gitconfig" ~/.gitconfig
ln -f -s "${currentPath}/gitignore_global" ~/.gitignore_global

# Copy bash user config
echo "Placing bash config"
bashConfigDest="${HOME}/.bashrc"
if [ $OSTYPE = "darwin*" ]; then
    bashConfigDest="${HOME}/.bash_profile"
fi
rm -f "${bashConfigDest}"
ln -f -s "${currentPath}/bash_config" "${bashConfigDest}"
source "${bashConfigDest}"

echo "Placing zsh config"
bashConfigDest="${HOME}/.zshrc"
rm -f "${bashConfigDest}"
ln -f -s "${currentPath}/zshrc" "${bashConfigDest}"
source "${bashConfigDest}"

echo "Done!"

