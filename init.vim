set shell=/bin/bash
set nocompatible

let g:python_host_prog  = 'python2'
let g:python3_host_prog = 'python3'

" Install Plug
if empty(glob('~/.vim/autoload/plug.vim')) || empty(glob('~/.config/nvim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    silent !cp -r ~/.vim/autoload ~/.config/nvim/
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/bundle')

" Plugins
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'enricobacis/vim-airline-clock'
Plug 'rodnaph/vim-color-schemes'
Plug 'dracula/vim', { 'as': 'dracula' }

Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-surround'
Plug 'kien/ctrlp.vim'
Plug 'eugen0329/vim-esearch'
Plug 'sareyko/neat.vim'

"Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'mhinz/vim-signify'

Plug 'rosenfeld/conque-term'

Plug 'ryanoasis/vim-devicons'
Plug 'Yggdroot/indentLine'
" Python
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
Plug 'zchee/deoplete-jedi'
Plug 'w0rp/ale'
Plug 'alfredodeza/pytest.vim'
Plug 'heavenshell/vim-pydocstring'
Plug 'fisadev/vim-isort'
Plug 'davidhalter/jedi-vim'
Plug 'ambv/black'
Plug 'lambdalisue/vim-cython-syntax'

" Go
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'zchee/deoplete-go', { 'do': 'make'}

" HTML
Plug 'alvan/vim-closetag'
Plug 'lepture/vim-jinja'
" Disabled plugins
" Snippets
"Plug 'majutsushi/tagbar'
"Plug 'garbas/vim-snipmate'		" Snippets manager
"Plug 'MarcWeber/vim-addon-mw-utils'	" dependencies #1
"Plug 'tomtom/tlib_vim'		" dependencies #2
"Plug 'honza/vim-snippets'		" snippets repo
call plug#end()

filetype on
filetype plugin on
filetype plugin indent on

" General
set backspace=indent,eol,start
let no_buffers_menu=1
" set mouse=a
set mousemodel=popup
set clipboard=unnamedplus
set ruler
set completeopt-=preview
set gcr=a:blinkon0
if has("gui_running")
	set cursorline
endif
set ttyfast


let g:deoplete#enable_at_startup = 1
syntax on

tab sball
set switchbuf=useopen

set noerrorbells visualbell t_vb= 
set novisualbell       

set encoding=UTF-8
set ls=2
set incsearch
set hlsearch
set number
set scrolloff=5

set showmatch

set updatetime=250
set autoread

set guioptions-=T    " тулбар

set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set smarttab
set autoindent

set undofile
set undodir=~/.vimundo/

" Airline
set laststatus=2
set background=dark

let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline#extensions#fugitiveline#enabled = 1
let g:airline_theme='dracula'
let g:airline#extensions#whitespace#show_message = 0

set termguicolors

" Theme
set t_Co=256
syntax enable
colorscheme dracula
hi Cursor guifg=green guibg=green
hi Cursor2 guifg=red guibg=red
set guicursor=n-v-c:block-Cursor/lCursor,i-ci-ve:ver25-Cursor2/lCursor2,r-cr:hor20,o:hor50
set guifont=Anonymice\ Nerd\ Font:h10
set guifont=Anonymice\ Powerline:h10
let g:indentLine_char = '▏'

" NerdTree
map <F3> :NERDTreeToggle<CR>
let NERDTreeIgnore=['\~$', '\.pyc$', '\.pyo$', '\.class$', 'pip-log\.txt$', '\.o$', '__pycache__']
let g:WebDevIconsNerdTreeGitPluginForceVAlign = 1
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:DevIconsEnableFoldersOpenClose = 1
let g:DevIconsEnableFolderPatternMatching = 1
" NerdComment
let g:NERDDefaultAlign = 'left'

let g:pydocstring_templates_dir = '~/.vim/pydocstring/templates/'

" Buffers
map <C-q> :bd<CR> 	   " CTRL+Q - close buffer 

" Autocomplete
let g:deoplete#enable_at_startup = 1
set completeopt-=preview
let g:go_auto_type_info = 0
let g:go_fmt_autosave = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1
let g:go_auto_sameids = 1
let g:go_fmt_command = "goimports"
let g:go_addtags_transform = "snakecase"
" Error and warning signs.
let g:ale_sign_error = '⤫'
let g:ale_sign_warning = '⚠'
let g:ale_linters = {'python': ['flake8'], 'go': ['go vet', 'go build']}
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_fix_on_save = 1
let g:ale_sign_column_always = 1
let g:ale_lint_on_text_changed = 'never'
let g:ale_python_black_options = '--skip-string-normalization --line-length 119'
let g:ale_python_isort_options = '-l 119'
let g:jedi#goto_command = "<leader>d"
let g:jedi#completions_enabled = 0
let g:jedi#use_tabs_not_buffers = 1
let g:jedi#show_call_signatures = "2"
let g:jedi#popup_on_dot = 0
let g:deoplete#sources#jedi#show_docstring=1

" Enable integration with airline.
let g:airline#extensions#ale#enabled = 1
let g:syntastic_python_flake8_args='--ignore=E501,W503 --max-line-length=119'
let g:ale_filetype_flake8_args='--ignore=E501,W503 --max-line-length=119'
let g:ale_python_flake8_options='--ignore=E501,W503 --max-line-length=119'


" Git
"let g:gitgutter_grep_command = 'grep'
"let g:gitgutter_eager = 0
if exists('&signcolumn')  " Vim 7.4.2201
  set signcolumn=yes
endif

let g:signify_sign_change = '~'
let g:signify_sign_delete = '-'
let g:signify_realtime = 1
let g:signify_sign_show_count = 0
let g:signify_sign_changedelete = g:signify_sign_change

" Black
let g:black_linelength = 119
let g:black_skip_string_normalization = 1
"autocmd BufWritePre *.py execute ':Black'

" Hotkeys
set noeb vb t_vb=
nnoremap <C-L> :nohlsearch<CR><C-L>
let g:vim_isort_map = '<C-i>'
nmap <silent> <C-e> <Plug>(pydocstring)


com! FormatJSON %!python -m json.tool

" ConqueTerm
nnoremap <F5> :ConqueTermSplit bash<CR>
let g:ConqueTerm_StartMessages = 0
let g:ConqueTerm_CloseOnEnd = 0

" Autocomplete on <Ctrl+Space>
inoremap <C-space> <C-x><C-o>
nnoremap <C-T> :Pytest project verbose<CR>

" Go
au FileType go nmap <C-m> <Plug>(go-def)

" Fugitive
nnoremap <C-S-B> :Gblame<CR>

nmap <C-H> :set invrelativenumber<CR>

" Languages support
" --- Python ---
"autocmd FileType python set completeopt-=preview

autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8
			\ formatoptions+=croq softtabstop=4 smartindent
			\ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
autocmd FileType pyrex setlocal expandtab shiftwidth=4 tabstop=8 softtabstop=4 smartindent 
			\ cinwords=if,elif,else,for,while,try,except,finally,def,class,with

" --- JavaScript ---
let javascript_enable_domhtmlcss=1
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd BufNewFile,BufRead *.json setlocal ft=javascript

" --- HTML ---
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags

" --- template language support (SGML / XML too) ---
autocmd FileType html,xhtml,xml,htmldjango,htmljinja,eruby,mako setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
autocmd bufnewfile,bufread *.rhtml setlocal ft=eruby
autocmd BufNewFile,BufRead *.mako setlocal ft=mako
autocmd BufNewFile,BufRead *.tmpl setlocal ft=htmljinja
autocmd BufNewFile,BufRead *.py_tmpl setlocal ft=python
au BufNewFile,BufRead *.html,*.htm,*.shtml,*.stm,*.json set ft=jinja
let html_no_rendering=1
let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.xml'
let g:closetag_xhtml_filenames = '*.xhtml,*.jsx'
let g:closetag_filetypes = 'html,xhtml,phtml'
let g:closetag_emptyTags_caseSensitive = 1
let g:closetag_shortcut = '>'
let g:closetag_close_shortcut = '<leader>>'
let g:sparkupNextMapping='<c-l>'

" --- CSS ---
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType css setlocal expandtab shiftwidth=4 tabstop=4 softtabstop=4

let &colorcolumn=121

if exists('g:loaded_webdevicons')
  call webdevicons#refresh()
endif

