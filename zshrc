export ZSH="/Users/fkhramov/.oh-my-zsh"

function include_settings
{
    if [ -f $HOME/$1 ]; then
        . $HOME/$1
    fi
}

ZSH_THEME="cloud"

ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"

plugins=(git colored-man-pages colorize pip python brew macos golang vim-interaction)

source $ZSH/oh-my-zsh.sh

# User configuration

alias vim="nvim"
alias vi="nvim"
alias oldvim="\vim"
alias vimdiff='nvim -d'
export EDITOR='nvim'

alias zshconfig="nvim ~/.zshrc"
alias ohmyzsh="nvim ~/.oh-my-zsh"

include_settings .bash_aliases

# Extended
include_settings .bashrc_extended



# Exports
export PATH=/usr/local/opt/python@3.10/Frameworks/Python.framework/Versions/3.10/bin:$PATH
export PATH="/usr/local/sbin:$PATH"
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
export MYVIMRC='~/.vimrc'
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
export PROMPT_DIRTRIM=2
export HISTFILESIZE=1000000
export HISTSIZE=1000000
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"


test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

[[ /usr/local/bin/kubectl ]] && source <(kubectl completion zsh)

